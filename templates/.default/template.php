<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
if (empty($arResult['YEARS'])) {
    ShowError('Нет новостей');
    return;
}
?>
    <link rel="stylesheet" href="<?=$templateFolder?>/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$templateFolder?>/style.css">
    <link rel="stylesheet" href="/bitrix/components/bitrix/main.pagenavigation/templates/.default/style.css">

    <nav class="nav nav-pills nav-fill">
        <? foreach ($arResult['YEARS'] as $yearData): ?>
            <? if ($yearData['ACTIVE']): ?>
                <a class="nav-link active" aria-current="page"
                   href="/news/?year=<?= $yearData['VALUE'] ?>"><?= $yearData['VALUE'] ?></a>
            <? else: ?>
                <a class="nav-link" href="/news/?year=<?= $yearData['VALUE'] ?>"><?= $yearData['VALUE'] ?></a>
            <? endif; ?>
        <? endforeach; ?>
    </nav>

    <div class="card-group">
        <? foreach ($arResult['NEWS'] as $newsItem):
            $resImage = CFile::ResizeImageGet(
                $newsItem["PREVIEW_PICTURE"],
                array("width" => 500, "height" => 500),
                BX_RESIZE_IMAGE_PROPORTIONAL
            );
            ?>
            <div class="card">
                <? if (!empty($resImage['src'])): ?>
                    <img src="<?= $resImage['src']; ?>"
                         alt="<?= $newsItem['NAME'] ?>">
                <? endif; ?>
                <div class="card-body">
                    <h5 class="card-title"><?= $newsItem['NAME'] ?></h5>
                    <p class="card-text"><?= $newsItem['PREVIEW_TEXT'] ?></p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Дата
                        публикации: <?= $newsItem['ACTIVE_FROM']->format('d.m.Y H:i:s'); ?></small>
                </div>
            </div>
        <? endforeach; ?>
    </div>

<?
$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "",
    array(
        "NAV_OBJECT" => $arResult['NAV']
    ),
    $component
);
?>