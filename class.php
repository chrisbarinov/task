<?
use \Bitrix\Main\Application;
use \Bitrix\Main\Data\Cache;
use \Bitrix\Main\Error;
use \Bitrix\Main\ErrorCollection;
use \Bitrix\Main\Context;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class CSimpleNewsComp extends CBitrixComponent
{
    private $errorCollection;

    public function onPrepareComponentParams($arParams)
    {
        $this->errorCollection = new ErrorCollection();
        if (!\Bitrix\Main\Loader::includeModule("iblock")) {
            $this->errorCollection->setError(
                new Error('Не подключен модуль инфоблоков')
            );
        }

        $request = Context::getCurrent()->getRequest();
        $arParams['YEAR'] = $request->getQuery("year");
        $arParams['PAGE'] = $request->getQuery("nav-more-news");

        return $arParams;
    }

    public function executeComponent()
    {
        if (count($this->errorCollection)) {
            foreach ($this->errorCollection as $error) {
                ShowError($error->getMessage());
            }
            return;
        }

        $this->arParams['CACHE_TIME'] = ($this->arParams['CACHE_TYPE'] == 'N') ? 0 : $this->arParams['CACHE_TIME'];
        $cache = Cache::createInstance();
        $cachePath = 'simplenewscomp';
        $cacheKey = md5($this->arParams['YEAR'] . $this->arParams['PAGE']);

        if ($cache->initCache($this->arParams['CACHE_TIME'], $cacheKey, $cachePath)) {
            $title = $cache->getVars();
            $this->setTitle($title);
            $cache->output(); // Выводим HTML пользователю в браузер
        } elseif ($cache->startDataCache()) {
            if ($this->arParams['CACHE_TYPE'] == 'A') {
                $taggedCache = Application::getInstance()->getTaggedCache(); // Служба пометки кеша тегами
                $taggedCache->startTagCache($cachePath);
                $taggedCache->registerTag('iblock_id_3');
            }
            $this->getNewsYears();
            if (!empty($this->arResult['YEARS'])) {
                $this->getNews();
            }
            $this->includeComponentTemplate();
            if ($this->arParams['CACHE_TYPE'] == 'A') {
                $taggedCache->endTagCache();
            }
            $cache->endDataCache($this->arResult['TITLE']);
        }
    }

    private function getNewsYears()
    {
        $newsItems = \Bitrix\Iblock\ElementTable::getList(array(
            'order' => array('ACTIVE_FROM' => 'DESC'), // сортировка
            'select' => array('ACTIVE_FROM'),
            'filter' => array('IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ACTIVE' => 'Y'),
        ));
        while ($newsItem = $newsItems->fetch()) {
            $year = $newsItem['ACTIVE_FROM']->format('Y');
            $this->arResult['YEARS'][$year] = ['VALUE' => $newsItem['ACTIVE_FROM']->format('Y'), 'ACTIVE' => false];
        }

        usort($this->arResult['YEARS'], function ($year1, $year2) {
            return ($year2['VALUE'] - $year1['VALUE']);
        });
    }

    private function getNews()
    {
        if (empty($this->arParams['YEAR'])) {
            $keyFirst = array_key_first($this->arResult['YEARS']);
            $this->arParams['YEAR'] = $this->arResult['YEARS'][$keyFirst]['VALUE'];
            $this->arResult['YEARS'][$keyFirst]['ACTIVE'] = true;
        } else {
            foreach ($this->arResult['YEARS'] as &$yearData) {
                if ($this->arParams['YEAR'] == $yearData['VALUE']) {
                    $yearData['ACTIVE'] = true;
                    break;
                }
            }
        }

        $this->arResult['NAV'] = new \Bitrix\Main\UI\PageNavigation("nav-more-news");
        $this->arResult['NAV']->allowAllRecords(true)
            ->setPageSize($this->arParams['NEWS_COUNT_PER_PAGE'])
            ->initFromUri();

        $newsItems = \Bitrix\Iblock\ElementTable::getList(array(
            'order' => array('ACTIVE_FROM' => 'DESC'), // сортировка
            'select' => array('ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'ACTIVE_FROM'),
            'filter' => array('IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', '>=ACTIVE_FROM' => '01.01' . $this->arParams['YEAR'] . " 00:00:00", '<=ACTIVE_FROM' => '31.12' . $this->arParams['YEAR'] . " 23:59:59"),
            'count_total' => true,
            'offset' => $this->arResult['NAV']->getOffset(),
            'limit' => $this->arResult['NAV']->getLimit(),
        ));

        $newsCount = $newsItems->getCount();
        $this->arResult['NAV']->setRecordCount($newsCount);

        while ($newsItem = $newsItems->fetch()) {
            $this->arResult['NEWS'][] = $newsItem;
        }

        $this->arResult['TITLE'] = "Список новостей (" . $newsCount . " шт.)";
        $this->setTitle($this->arResult['TITLE']);
    }

    private function setTitle($title)
    {
        global $APPLICATION;
        $APPLICATION->SetTitle($title);

    }
}

?>