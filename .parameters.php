<?
$arComponentParameters = [
    "GROUPS" => [
        "BASE" => [
            "NAME" => "Основные параметры",
            "SORT" => "100",
        ],
        "DATA_SOURCE" => [
            "NAME" => "Источник данных",
            "SORT" => "200",
        ],
        "CACHE_SETTINGS" => [
            "NAME" => "Настройки кеширования",
            "SORT" => "300",
        ],
    ],
    "PARAMETERS" => [
        "IBLOCK_ID" => [
            "PARENT" => "DATA_SOURCE",
            "NAME" => 'ID инфоблока новостей',
            "TYPE" => "STRING",
        ],
        "NEWS_COUNT_PER_PAGE" => [
            "PARENT" => "DATA_SOURCE",
            "NAME" => 'Число новостей на странице',
            "TYPE" => "STRING",
        ],
        "CACHE_TYPE" => [
            "PARENT" => "CACHE_SETTINGS",
            "NAME" => 'Тип кеширования',
            "TYPE" => "LIST",
            "VALUES" => [
                "A" => "Авто + Управляемое: автоматически обновляет кеш компонентов в течение заданного времени или при изменении данных",
                "Y" => "Кешировать: для кеширования необходимо определить время кеширования",
                "N" => "Не кешировать: кеширования нет в любом случае",
            ],
        ],
        "CACHE_TIME" => [
            "PARENT" => "CACHE_SETTINGS",
            "NAME" => 'Время кеширования',
            "TYPE" => "STRING"
        ]

    ]
];
?>

